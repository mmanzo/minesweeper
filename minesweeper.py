#!/usr/bin/python3

import sys, os
import numpy as np
from ast import literal_eval


class Board:
    def __init__(self, size, tile_matrix):
        self.size = size
        self.tile_matrix = tile_matrix

    def get_tile(self, x, y):
        return self.tile_matrix[x][y]

    def expose_board(self):
        """
        matrix = []
        for x in range(self.size):
            row = []
            for y in range(self.size):
                if self.tile_matrix[x][y].get_is_mine() == True:
                    row.append(1)
                else:
                    row.append(0)
            matrix.append(row)
        for row in matrix:
            print(row)
        """
        for x in range(self.size):
            row_output = ""
            for y in range(self.size):
                tile = self.tile_matrix[x][y]
                if tile.get_is_mine():
                    row_output += "*"
                else:
                    row_output += "-"
            print(row_output)

    def set_board_neighbors(self):
        for x in range(self.size):
            row_curr = self.tile_matrix[x]
            for y in range(self.size):
                tileNW = None
                tileN = None
                tileNE = None
                
                tileSW = None
                tileS = None
                tileSE = None
                
                tileW = None
                tileE = None
                # Check neighbors top
                if x > 0:
                    row_above = self.tile_matrix[x - 1]
                    tileN = row_above[y]
                    # Check neighbors top-left
                    if y > 0:
                        tileNW = row_above[y - 1]
                    # Check check neighbors top-right
                    if y < self.size - 1:
                        tileNE = row_above[y + 1]

                # Check neighbors bottom
                if x < self.size - 1:
                    row_below = self.tile_matrix[x + 1]
                    tileS = row_below[y]
                    # Check neighbors bottom-left
                    if y > 0:
                        tileSW = row_below[y - 1]
                    # Check check neighbors bottom-right
                    if y < self.size - 1:
                        tileSE = row_below[y + 1]

                # Check neighbors left
                if y > 0:
                    tileW = row_curr[y - 1]
                # Check neighbors right
                if y < self.size - 1:
                    tileE = row_curr[y + 1]

                self.tile_matrix[x][y].set_neighbors(tileNW, tileN, tileNE, tileSW, tileS, tileSE, tileW, tileE)
    
    def set_tile(self, x, y, tile):
        self.tile_matrix[x][y] = tile


class Tile:
    def __init__(self, is_mine, is_selected):
        self.is_mine = is_mine
        self.is_selected = is_selected
        self.mine_count = None
        self.N = None
        self.NE = None
        self.E = None
        self.SE = None
        self.S = None
        self.SW = None
        self.W = None
        self.NW = None

    def get_is_mine(self):
        return self.is_mine

    def get_is_selected(self):
        return self.is_selected

    def set_selected(self, selected):
        self.is_selected = selected

    def set_neighbors(self, NW, N, NE, SW, S, SE, W, E):
        self.NW = NW
        self.N = N
        self.NE = NE
        self.SW = SW
        self.S = S
        self.SE = S
        self.W = W
        self.E = E

    def get_neighbor(self, direction):
        if direction == "N":
            return self.N
        elif direction == "NE":
            return self.NE
        elif direction == "E":
            return self.E
        elif direction == "SE":
            return self.SE
        elif direction == "S":
            return self.S
        elif direction == "SW":
            return self.SW
        elif direction == "W":
            return self.W
        elif direction == "NW":
            return self.NW

    def get_neighboring_mine_count(self):
        count = 0
        neighbors = []
        if self.NW is not None:
            neighbors.append(self.NW)
        if self.N is not None:
            neighbors.append(self.N)
        if self.NE is not None:
            neighbors.append(self.NE)
        if self.SW is not None:
            neighbors.append(self.SW)
        if self.S is not None:
            neighbors.append(self.S)
        if self.SE is not None:
            neighbors.append(self.SE)
        if self.W is not None:
            neighbors.append(self.W)
        if self.E is not None:
            neighbors.append(self.E)
        for neighbor in neighbors:
            if neighbor.get_is_mine() == True:
                count += 1
        return count

    def set_mine_count(self, count):
        self.mine_count = count

    def get_mine_count(self):
        return self.mine_count


def initialize_board(size):
    # First, randomly generate 1d array (size*size) of randomly seeded 0s and 1s
    # 0 represents a clear tile
    # 1 represents a mine tile
    N = size*size
    K = int(.3 * N) # K mines (ones), N - K clear tiles (zeroes)
    arr = np.array([1] * K + [0] * (N-K))
    np.random.shuffle(arr)

    # Use random 1d array to create 2d matrix representation of game board
    binary_matrix = np.reshape(arr, (-1, size))
    tile_matrix = []
    for x in range(size):
        row = []
        for y in range(size):
            is_mine = False
            if binary_matrix[x][y] == 1:
                is_mine = True
            tile = Tile(is_mine, False)
            row.append(tile)
        tile_matrix.append(row)
    board = Board(size, tile_matrix)
    board.set_board_neighbors()
    board.expose_board()
    return board


def print_player_board(size, board):
    for x in range(size):
        row_output = ""
        for y in range(size):
            tile = board.get_tile(x, y)
            if tile.get_is_selected():
                row_output += str(tile.get_mine_count())
            else:
                row_output += "-"
        print(row_output)


def get_player_move(size, master_board, player_board):
    try:
        player_input = input("Select a tile and enter as (x,y): ")
        move = literal_eval(player_input)
        return move
    except Exception:
        print("Invalid input. Please enter move in the form (x,y).")
        run_game(size, master_board, player_board, True)


def is_valid_move(size, move, master_board, player_board):
    try:
        x = move[0]
        y = move[1]
        if x < 0 or x >= size or y < 0 or y >= size:
            print("Invalid move. Please enter another move.")
            run_game(size, master_board, player_board, True)
        selected_tile = player_board.get_tile(x, y)
        if selected_tile.get_is_selected():
            print("Invalid move. Please enter another move.")
            run_game(size, master_board, player_board, True)
        return True
    except Exception:
        print("Invalid move. Please enter another move.")
        run_game(size, master_board, player_board, True)


def run_game(size, master_board, player_board, player_alive):
    while player_alive == True:
        # Display player_board
        print_player_board(size, player_board)
        
        # Prompt player for move
        move = get_player_move(size, master_board, player_board)

        # Check if valid move. Recursively calls run_game if invalid
        if is_valid_move(size, move, master_board, player_board) == True:
            # Evaluate move
            x = move[0]
            y = move[1]
            selected_tile = player_board.get_tile(x, y)
            if selected_tile.get_is_mine() == True:
                # Display player_board TODO
                print("Sorry, you blew up!")
                player_alive = False
                master_board.expose_board()
                break

            # Update player_board
            # Get neighbors to calculate mine count
            count = selected_tile.get_neighboring_mine_count()
            selected_tile.set_selected(True)
            selected_tile.set_mine_count(count)
            player_board.set_tile(x, y, selected_tile)
            
            run_game(size, master_board, player_board, True)


def main():
    size = 3
    board = initialize_board(size)
    run_game(size, board, board, True)


if __name__ == "__main__":
    main()
